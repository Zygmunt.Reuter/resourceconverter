﻿

#include <iostream>
#include <fstream>
#include <string>
#include <locale>
#include <codecvt>
#include <sstream>
#include <vector>
#include <assert.h>
#include <unordered_map>
#include <algorithm>
#include <iomanip>


inline void ltrim(std::wstring& s) {
    auto pos = s.find_first_not_of(L" \n\r\t");
    if(pos != std::string::npos)
        s = s.substr(pos);
}
inline void ptrim(std::wstring& s) {
    auto pos = s.find_last_not_of(L" \n\r\t");
    if (pos != std::string::npos)
        s = s.substr(0, pos + 1);
}

void trim(std::wstring& str) {
    ltrim(str);
    ptrim(str);
}



std::vector<std::wstring> splitToVector(const std::wstring& str, wchar_t delimiter, bool ignoreEmpty = false) {
    std::wstringstream ss(str);
    std::wstring singleStr;
    std::vector<std::wstring> ret;
    while (std::getline(ss, singleStr, delimiter))
    {
        if (ignoreEmpty && singleStr.empty())
            continue;
        ret.push_back(singleStr);
    }
    return ret;
}

std::vector< std::pair<std::wstring, std::wstring>> SplitStringTableLine(std::wstring& str) 
{
    std::wstringstream ss(str);
    std::wstring temp;
    std::vector<std::wstring> helper;
    while (std::getline(ss, temp, L'\"'))
    {
        helper.push_back(temp);
    }

    assert(helper.size() % 2 == 0);// is even

    std::vector< std::pair<std::wstring, std::wstring>> ret;

    for (auto i = 0; i < helper.size(); i += 2) 
    {
        if (helper[i].empty()) 
        {
            ret.rbegin()->second += L"\"";
            ret.rbegin()->second += helper[i + 1];
            ret.rbegin()->second += L"\"";
            continue;
                
        }
        trim(helper[i]);
        ret.push_back(std::make_pair(helper[i], helper[i + 1]));
    }
    return ret;
}

std::vector < std::pair<std::wstring, std::wstring>> GetStringTableFromFile(std::wstring path) 
{
    std::vector < std::pair<std::wstring, std::wstring>> strTable;

    std::wifstream file(path, std::ios::binary);
    file.imbue(std::locale(file.getloc(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::little_endian>));
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    if (!file.is_open()) 
    {
        std::wcout << L"Error opening file." << path <<std::endl;
        return strTable;
    }


    std::wstring utf16Data;

    union StringTableSectionHelper {
        struct
        {
            unsigned short STRINGTABLE : 1;
            unsigned short BEGIN : 1;
        } X;
        unsigned short isStrinTableSection : 2;
    };

    StringTableSectionHelper isStringTableSection;
    isStringTableSection.isStrinTableSection = 0;

    std::wstring strTabSection;
    while (std::getline(file, utf16Data)) {
        std::string utf8Data = converter.to_bytes(utf16Data);

        trim(utf16Data);

        if (utf16Data.compare(L"STRINGTABLE") == 0)
        {
            isStringTableSection.X.STRINGTABLE = 1;
            continue;
        }
        if (isStringTableSection.X.STRINGTABLE == 1 && utf16Data.compare(L"BEGIN") == 0)
        {
            isStringTableSection.X.BEGIN = 1;
            continue;
        }

        if (isStringTableSection.isStrinTableSection != 0b00000011)
            continue;


        if (utf16Data.compare(L"END") == 0)
        {
            isStringTableSection.isStrinTableSection = 0;

            auto vec = SplitStringTableLine(strTabSection);
            strTabSection.clear();

            strTable.insert(strTable.end(), vec.begin(), vec.end());
            continue;
        }
        strTabSection += utf16Data;

    }
    return strTable;
}

void PrintStringTableToFile(std::wstring path, std::wstring langSignature, std::vector < std::pair<std::wstring, std::wstring>> stringtable) 
{

    std::wofstream outFile(path, std::ios::binary);
    // 
    outFile.imbue(std::locale(outFile.getloc(), new std::codecvt_utf8<wchar_t, 0x10ffff, std::generate_header>));

    outFile << L"{" << std::endl;
    outFile << L"\t\"@@locale\": \""<< langSignature <<"\"," << "\r\n";


    for (auto i = 0U; i < stringtable.size(); ++i)
    {
        outFile << L"\t\"" << stringtable[i].first << "\": {" << L"\"" << stringtable[i].second << L"\"}," << "\r\n";
        //outFile << L"\t\"@" << stringtable[i].first << "\": {\"description\": \"\" }," << "\r\n";
    }
    outFile << L"}" << "\r\n";
}

std::unordered_map<std::wstring, uint32_t> GetIDS_MapFromHeaderFile(const std::wstring& path) 
{
    std::unordered_map<std::wstring, uint32_t> IDS_map;

    std::wifstream headerFile(path);
    if (!headerFile.is_open()) 
    {
        std::wcout <<L"Error opening file." << path;
        return IDS_map;
    }

    std::wstring str;
    while (std::getline(headerFile, str))
    {
        auto splited = splitToVector(str, L' ', true);

        const std::wstring idsPrefix = L"IDS_";
        if (splited.size() >= 3 // vector structure should be similar to  {#define; IDB_BALANCE_FULL_CHK; 105}
            && splited[0] == L"#define" // each line should start with #define
            && splited[1].substr(0, idsPrefix.size()) == idsPrefix // IDS_ are important
            )
        {
            std::wstring IdsName = splited[1];
            std::wstring IdsNumberStr = splited[2];
            uint32_t IdsNumber = stoi(IdsNumberStr);

            auto pos = IDS_map.insert(std::make_pair(IdsName, IdsNumber));
            if (!pos.second) 
                std::wcout << pos.first->first << "Eleemnt redefinition :" << pos.first->first <<  std::endl;
        }
    }
    return IDS_map;
}

void PrintIDS_MapToFile(const std::wstring& path, std::unordered_map<std::wstring, uint32_t> map)
{
    std::wofstream outFile(path);
    if (!outFile.is_open())
        throw std::runtime_error("Error opening file.");
    
    outFile << L"syntax = \"proto3\";" << std::endl;
    
    outFile << L"enum IDS {" << std::endl;


    std::vector<std::pair<std::wstring, uint32_t>> sortedIDS(map.begin(), map.end());
    std::sort(sortedIDS.begin(), sortedIDS.end(), [](const std::pair<std::wstring, uint32_t>& a, const std::pair<std::wstring, uint32_t>& b)
        {
            return a.second < b.second;
        });

    for (auto& it : sortedIDS)
    {
        outFile << "\t" << it.first << " = " << it.second << ";" << std::endl;
    }

    outFile << L"}" << std::endl;
}

std::vector < std::pair<std::wstring, std::wstring>> GetCommonPartofResource_hAndRCFile(
    const std::unordered_map<std::wstring, uint32_t>& resourece_hIds,
    const std::vector < std::pair<std::wstring, std::wstring>>& rcfileIds,
    const std::wstring& rcFilePath) 
{

    std::vector < std::pair<std::wstring, std::wstring>> strTableBasedOn_resource_h;
    for (auto& ids : resourece_hIds)
    {
        auto it = std::find_if(rcfileIds.begin(), rcfileIds.end(),
            [&ids](const std::pair<std::wstring, std::wstring>& pair) ->bool {return pair.first == ids.first; });

        if (it != rcfileIds.end())
            strTableBasedOn_resource_h.push_back(*it);
        else
            std::wcout << L"\tCan not find " << ids.first << " parametr in " << rcFilePath << std::endl;
    }

    // check that left some element in resource file 
    decltype(strTableBasedOn_resource_h) difMap;

    std::sort(strTableBasedOn_resource_h.begin(), strTableBasedOn_resource_h.end());
    std::set_difference(rcfileIds.begin(), rcfileIds.end(), strTableBasedOn_resource_h.begin(), strTableBasedOn_resource_h.end(), std::back_inserter(difMap));
    for (auto& it : difMap)
        std::wcout << L"Ids: " << it.first << L", exist in " << rcFilePath << L" but no exist in resource.h"<< std::endl;

    return strTableBasedOn_resource_h;
}


int wmain(int argc, wchar_t* argv[]) {

    if ((argc - 2 ) %2 !=  0) // to do
    {
        std::cout << "You should pass paths to rc files as a program parameters " << std::endl;
        std::cout << "examle: " << std::endl;
        std::cout << "\tResourceConverter.exe resource.h en KVResEU.rc cz KVResCZE.rc" << std::endl;
    }
    std::wstring resHeaderPath = argv[1];

    std::wcout << L"Reading " << resHeaderPath << std::endl;
    auto IDS_FromResource_h = GetIDS_MapFromHeaderFile(resHeaderPath);

    if (IDS_FromResource_h.empty()) return 0;

    std::wcout << resHeaderPath << " conversion to proto " << std::endl;
    auto protoPath = resHeaderPath.substr(0, resHeaderPath.find_last_of(L'.')) + L".proto";
    PrintIDS_MapToFile(protoPath, IDS_FromResource_h);
   
    for (int i = 2; i < argc; i += 2) {

        std::wstring langSignature = argv[i];
        std::wstring rcFile = argv[i + 1];

        std::wcout << rcFile << " conversion to arb file" << std::endl;


        std::vector < std::pair<std::wstring, std::wstring>> strTable = GetStringTableFromFile(rcFile);

       /* if (strTable.empty()) continue;
        std::sort(strTable.begin(), strTable.end());

        auto strTableBasedOn_resource_h = GetCommonPartofResource_hAndRCFile(IDS_FromResource_h, strTable, rcFile);*/
       
        PrintStringTableToFile(rcFile += L".arb", langSignature, strTable);
    }


     return 0;
}